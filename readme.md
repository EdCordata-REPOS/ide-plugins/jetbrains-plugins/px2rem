# JetBrains px2rem

Convert px to rem, using simple shortcut (default `Alt+R`) after or while selecting value in css

### TODO's
* Make this plugin work with multiple carets
* Add settings page and add `1rem = 16px`
* Add other options, not just px->rem (should probably rename project after that)
