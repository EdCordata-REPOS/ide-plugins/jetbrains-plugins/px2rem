import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.SelectionModel;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.editor.Editor;
import java.text.DecimalFormat;


public class px2remAction extends AnAction {

    public void actionPerformed(AnActionEvent e) {
        Editor editor                 = (Editor)  e.getRequiredData(CommonDataKeys.EDITOR);
        Project project               = (Project) e.getRequiredData(CommonDataKeys.PROJECT);
        SelectionModel selectionModel = editor.getSelectionModel();
        Document document             = editor.getDocument();

        int start     = selectionModel.getSelectionStart();
        int end       = selectionModel.getSelectionEnd();
        String sel    = selectionModel.getSelectedText();
        boolean found = false;

        if (selectionModel.hasSelection()) {
            found = true;
        } else {
            boolean search = true;
            int i = 0;

            // go back by 1 character in selection and search for ':' or ' ' character, cos that means we have
            // found the right value, example: 100px
            while (search) {
                start = start - 1;
                i = i + 1;

                if (i > 50) { // exit loop early. We definitely won't have more than 50 character long variable
                    found  = false;
                    search = false;
                } else {

                    // check if we reached file start
                    if (start < 0) {
                        found  = false;
                        search = false;
                    } else {
                        selectionModel.setSelection(start, end);
                        sel = selectionModel.getSelectedText();

                        // check if first char is not number or dot. If so, we have our value
                        if ( !Character.isDigit(sel.charAt(0)) && sel.charAt(0) != '.' )
                            start = start + 1;
                            selectionModel.setSelection(start, end);
                            sel    = selectionModel.getSelectedText();
                            found  = true;
                            search = false;
                        }
                    }
                }
            }
        }

        if (found) {
            if (sel.contains("px")) {

                // convert px to rem;
                double px        = Double.parseDouble(sel.replace("px", ""));
                double rem       = (px * 0.0625); // 1rem = 16px
                DecimalFormat df = new DecimalFormat("#####.#####");
                String rem_str   = df.format(rem) + "rem";

                // replace px with rem
                final int final_start = start;
                final int final_end   = end;
                WriteCommandAction.runWriteCommandAction(project, () -> {
                    document.replaceString(final_start, final_end, rem_str);
                });

                // reset selection
                selectionModel.removeSelection();

                // move cursor after our new value
                CaretModel caretModel     = editor.getCaretModel();
                final int final_selection = (start + rem_str.length());
                caretModel.moveToOffset(final_selection);
            }
        }
    }
}
